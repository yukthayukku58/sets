# Create a set
fruits = {"apple", "banana", "cherry"}

# Add elements to a set
fruits.add("durian")
print(fruits) 

# Remove an element from a set
fruits.remove("banana")
print(fruits)  

# Set length
length = len(fruits)
